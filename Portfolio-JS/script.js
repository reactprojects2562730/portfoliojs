let backVideo = document.getElementById("backVideo");
let btn = document.getElementById("btn");


btn.onclick = function () {
    if (backVideo.paused) {
        backVideo.play();
        backVideo.style.display = 'block';
        btn.src = './images/play.png';
    }
    else {
        backVideo.pause();
        backVideo.style.display = 'none';
        btn.src = './images/pause.png';
    }
}

